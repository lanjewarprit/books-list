package com.pritam.newsapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pritam.newsapp.R
import com.pritam.newsapp.data.BoksResult
import com.pritam.newsapp.databinding.RowBooksTitleBinding

class BooksTitleAdapter(val results: List<BoksResult>) : RecyclerView.Adapter<BooksTitleAdapter.ViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.row_books_title, viewGroup, false)
        val binding: RowBooksTitleBinding = RowBooksTitleBinding.bind(view)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        val bookTitle: BoksResult = results!![i]
        viewHolder.rowBooksTitleBinding.tvBookTitle.setText(bookTitle.title)
    }

    override fun getItemCount(): Int {
        return results!!.size
    }

    class ViewHolder(itemView: RowBooksTitleBinding) : RecyclerView.ViewHolder(itemView.getRoot()) {
        var rowBooksTitleBinding: RowBooksTitleBinding

        init {
            rowBooksTitleBinding = itemView
        }
    }
}