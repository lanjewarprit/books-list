package com.pritam.newsapp.dagger.component

import com.pritam.newsapp.dagger.facoty.ViewModelModule
import com.pritam.newsapp.dagger.module.BooksModule
import com.pritam.newsapp.view.BooksListActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [BooksModule::class, ViewModelModule::class])
interface BooksComponent {
    fun inject(activity: BooksListActivity)
}