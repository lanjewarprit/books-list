package com.pritam.newsapp.data

data class BoksResult(
    val bookshelves: List<String>,
    val download_count: Int,
    val id: Int,
    val languages: List<String>,
    val media_type: String,
    val subjects: List<String>,
    val title: String
)