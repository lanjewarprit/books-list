package com.pritam.newsapp.data


data class BooksResponseData(val count: Int,
                             val results: List<BoksResult>)