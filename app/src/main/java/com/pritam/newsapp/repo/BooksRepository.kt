package com.pritam.newsapp.repo

import androidx.lifecycle.MutableLiveData
import com.pritam.newsapp.data.BooksResponseData

import com.pritam.newsapp.retroshare.APIServices
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class BooksRepository @Inject constructor(private val api: APIServices) {

    private val mCompositeDisposable = CompositeDisposable()

    private val progress = MutableLiveData<Boolean>()

    //progress
    fun getProgress() = progress

    fun getCompositeDisposable() = mCompositeDisposable

    /**
     * get book list from server
     */
    fun getBookListFromServer(): MutableLiveData<BooksResponseData> {
        val data = MutableLiveData<BooksResponseData>()
        api.getBookList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { progress.postValue(true) }
            .subscribeBy(
                onNext = {
                    progress.postValue(false)
                    data.value = it
                },
                onError = {
                    progress.postValue(false)
                    data.value = BooksResponseData(0, listOf())
                }
            ).addTo(mCompositeDisposable)
        return data
    }

}