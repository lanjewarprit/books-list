package com.pritam.newsapp.retroshare

import com.pritam.newsapp.data.BooksResponseData
import io.reactivex.Observable
import retrofit2.http.GET

interface APIServices {
    @GET("books")
    fun getBookList(): Observable<BooksResponseData>
}