package com.pritam.newsapp.utils

import android.app.Application

import com.pritam.newsapp.dagger.component.BooksComponent
import com.pritam.newsapp.dagger.component.DaggerBooksComponent
import com.pritam.newsapp.dagger.module.BooksModule

class AppApplication : Application() {

    lateinit var networkComponent: BooksComponent

    override fun onCreate() {
        super.onCreate()
        networkComponent = DaggerBooksComponent
                .builder()
                .booksModule(BooksModule)
                .build()
    }

}