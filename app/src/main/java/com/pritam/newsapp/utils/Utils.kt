package com.pritam.newsapp.utils

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import com.pritam.newsapp.interfaces.OnRetryListener

object Utils {

    /**
     * internet connection check here and
     * @param context
     * @return
     */
    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnected
    }

    /**
     * pop up if internet connection not available
     * @param mContext
     * @param onRetryListener
     */
    fun internetConnectionPopUp(mContext: Context, onRetryListener: OnRetryListener) {
        val alert = AlertDialog.Builder(mContext)
        alert.setTitle("Internet Connection")
        alert.setMessage("Internet connection not available, Please try again")
        alert.setCancelable(false)
        alert.setPositiveButton(
            "Retry"
        ) { dialog, whichButton -> onRetryListener.retry() }
        alert.setNegativeButton(
            "Exit"
        ) { dialog, whichButton -> (mContext as Activity).finish() }
        alert.show()
    }

    /**
     * alert pop up here
     */
    fun alertPopUp(
        mContext: Context?,
        title: String?,
        message: String?,
        onRetryListener: OnRetryListener
    ) {
        val alert = AlertDialog.Builder(
            mContext!!
        )
        alert.setTitle(title)
        alert.setMessage(message)
        alert.setCancelable(false)
        alert.setPositiveButton(
            "Ok"
        ) { dialog, whichButton -> onRetryListener.retry() }
        alert.show()
    }
}