package com.pritam.newsapp.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.pritam.newsapp.adapter.BooksTitleAdapter
import com.pritam.newsapp.data.BoksResult
import com.pritam.newsapp.databinding.ActivityBooksListBinding
import com.pritam.newsapp.interfaces.OnRetryListener
import com.pritam.newsapp.utils.AppApplication
import com.pritam.newsapp.utils.Utils
import com.pritam.newsapp.viewmodel.UserViewModel
import javax.inject.Inject

class BooksListActivity : AppCompatActivity() {
    var activityBooksTitleBinding: ActivityBooksListBinding? = null
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mViewModel: UserViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (application as AppApplication).networkComponent.inject(this)
        initView()
        mViewModel.getData()
        observeUser()
        observeProgress()
        observeError()
    }

    /**
     * initialise view binding here
     */
    private fun initView() {
        activityBooksTitleBinding = ActivityBooksListBinding.inflate(layoutInflater)
        setContentView(activityBooksTitleBinding!!.root)
    }

    /**
     * get book list
     * network call here
     */
    private fun observeUser() {
        if(Utils.isNetworkConnected(this)) {
            mViewModel.getUserModel().observe(this, Observer {
                it?.let {
                    setNearestRestaurantAdapter(it.results)
                }
            })
        }else Utils.internetConnectionPopUp(this, object : OnRetryListener{
            override fun retry() {
                observeUser()
            }
        })

    }

    /**
     * progress bar here
     * display progress bar visible till data come
     */
    private fun observeProgress() {
        mViewModel.getProgress().observe(this, Observer {
            activityBooksTitleBinding!!. newsProgress.visibility = it.getVisibility()
        })
    }

    /**
     * if the something went worng with server
     * alret message here
     */
    private fun observeError() {
        mViewModel.getErrors().observe(this, Observer {
            it?.let {
                Utils.alertPopUp(this,"Info","Something went wrong, Please try again", object : OnRetryListener{
                    override fun retry() {
                        observeUser()
                    }
                })
            }
        })
    }

    /**
     * book list adapter here
     *
     * @param results
     */
    private fun setNearestRestaurantAdapter(results: List<BoksResult>) {
        if (!results.isEmpty()) {
            val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            activityBooksTitleBinding!!.recyclerViewNews.setHasFixedSize(false)
            activityBooksTitleBinding!!.recyclerViewNews.setLayoutManager(linearLayoutManager)
           val booksTitleAdapter = BooksTitleAdapter( results)
            activityBooksTitleBinding!!.recyclerViewNews.setAdapter(booksTitleAdapter)

        }
    }

    /**
     * handle visibility here
     */
    private fun Boolean?.getVisibility(): Int = if (this != null && this) View.VISIBLE else View.GONE

}