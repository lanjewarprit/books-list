package com.pritam.newsapp.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.pritam.newsapp.R

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        handleLaunchActivity()
    }

    /**
     * 2 sec timer here
     * launch near by BooksTitleActivity
     */
    private fun handleLaunchActivity() {
        Handler().postDelayed({
            val intent = Intent(this@SplashActivity, BooksListActivity::class.java)
            startActivity(intent)
            finish()
        }, 2000)
    }
}