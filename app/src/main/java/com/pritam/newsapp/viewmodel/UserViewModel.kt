package com.pritam.newsapp.viewmodel


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pritam.newsapp.data.BooksResponseData
import com.pritam.newsapp.repo.BooksRepository

import javax.inject.Inject

class UserViewModel @Inject constructor(private val mBooksRepo: BooksRepository) : ViewModel() {

    private var userModel = MutableLiveData<BooksResponseData>()
    private val error = MutableLiveData<String>()

    fun getProgress() = mBooksRepo.getProgress()

    fun getUserModel(): MutableLiveData<BooksResponseData> = userModel

    fun getErrors() = error

    fun getData() {
        userModel.value?.let {
            return
        }
        userModel = mBooksRepo.getBookListFromServer()
    }

    override fun onCleared() {
        super.onCleared()
        mBooksRepo.getCompositeDisposable().clear()
        mBooksRepo.getCompositeDisposable().dispose()
    }
}
